package io.metersphere.v3.commons.constants;

public enum RequestMethod {
    GET, POST
}
