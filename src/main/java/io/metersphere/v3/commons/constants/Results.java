package io.metersphere.v3.commons.constants;

public class Results {
    public static final String COMPLETED = "Completed";
    public static final String FAILURE = "Failure";
    public static final String SUCCESS = "Success";
    public static final String API = "api";
    public static final String STOPPED = "STOPPED";
    public static final String ERROR = "ERROR";
}
